var gulp = require('gulp'),
	less = require('gulp-less'),
	sourcemaps = require('gulp-sourcemaps'),
	notify = require('gulp-notify'),
	notifier = require('node-notifier'),
	minjs = require('gulp-uglify'),
	mincss = require('gulp-clean-css'),
	prettify = require('gulp-html-prettify'),
	svgmin = require('gulp-svgmin'),
	tinypng = require('gulp-tinypng-compress'),
	debug = require('gulp-debug'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	nunjucksRender = require('gulp-nunjucks-render'),
	rigger = require('gulp-rigger'),
	spritesmith = require('gulp.spritesmith'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	connect = require('gulp-connect-multi')();
	path = {
		src: {
			js: 'scripts/app.js',
			less: 'css/main.less',
			svg: 'img-src/SVG/*.svg',
			twig: 'templates/*.+(twig)',
			icons: 'img-src/icons/*.png',
			img: 'img-src/**'
		},
		build: {
			js: 'scripts/',
			css: 'css/',
			img: 'img/',
			svg: 'img/SVG/',
			html: ''
		},
		watch: {
			js: [
				'scripts/**/*.js',
				'!scripts/app.min.js'
			],
			twig: 'templates/**/*.+(twig)',
			css: 'css/**/*.less'
		}
	};

gulp.task('connect', connect.server({
	root: [__dirname],
	port: 1357,
	livereload: true,
	open: {
		browser: 'google-chrome'
	}
}));

gulp.task('tinypng', function () {
	gulp.src(path.src.img)
		.pipe(tinypng({
			key: '3Pa0gDKr5jxc9eSOh2ih_Hyt1LPcRG-N',
			checkSigs: true,
			sigFile: 'images/.tinypng-sigs',
			log: true
		}))
		.pipe(gulp.dest(path.build.img));
});

gulp.task('imagemin', function () {
	gulp.src(path.src.img)
		.pipe(imagemin({
				progressive: true,
				optimizationLevel: 1,
				svgoPlugins: [
					{removeViewBox: false},
					{removeDoctype: true},
					{removeComments: true},
					{cleanupNumericValues:
						{floatPrecision: 2}
					},
					{convertColors: {
							names2hex: false,
							rgb2hex: false
						}
					}],
				use: [pngquant()]
			}
		))
		.pipe(gulp.dest(path.build.img));
});

gulp.task('sprite', function () {
	var spriteData = gulp.src(path.src.icons)
		.pipe(spritesmith({
			imgName: 'iconostas.png',
			imgPath: '../img/iconostas.png',
			padding: 5,
			retinaSrcFilter: 'img-src/icons/*@2x.png',
			retinaImgName: 'iconostas@2x.png',
			retinaImgPath: '../img/iconostas@2x.png',
			cssName: 'iconostas.less'
		}));
	spriteData.img.pipe(gulp.dest('img-src/'));
	spriteData.css.pipe(gulp.dest(path.build.css));
});

gulp.task('svgmin', function () {
	var prefix = '123';
	return gulp.src(path.src.svg)
		.pipe(debug({title: 'SVG:'}))
		.pipe(svgmin({
			plugins: [{
				removeDoctype: true
			}, {
				removeComments: true
			}, {
				cleanupNumericValues: {
					floatPrecision: 2
				}
			}, {
				convertColors: {
					names2hex: false,
					rgb2hex  : false
				}
			}, {
				cleanupIDs: {
					prefix: prefix + '-',
					minify: true
				}
			}]
		}))
		.pipe(gulp.dest(path.build.svg));
});

// Html
gulp.task('nunjucks', function() {
	// Gets .html and .nunjucks files in pages
	return gulp.src(path.src.twig)
		.pipe(nunjucksRender({
			path: ['templates']
		}))
		.on('error', function(err) {
			notifier.notify({
				'title': "Error",
				'message': err.message
			});
			console.log(err.message);
			return false;
		})
		.pipe(prettify({indent_char: '\t', indent_size: 1}))
		.pipe(gulp.dest(path.build.html))
		.pipe(connect.reload());
});

gulp.task('nunjucks', function() {
	// Gets .html and .nunjucks files in pages
	return gulp.src(path.src.twig)
		.pipe(nunjucksRender({
			path: ['templates']
		}))
		.on('error', function(err) {
			notifier.notify({
				'title': "Error",
				'message': err.message
			});
			console.log(err.message);
			return false;
		})
		.pipe(debug({title: 'Twig:'}))
		.pipe(gulp.dest(path.build.html))
		.pipe(connect.reload());
});

gulp.task('less', function() {
	gulp.src(path.src.less)
		.pipe(sourcemaps.init())
		.pipe(less())
		// .pipe(debug({title: 'Less:'}))
		.on('error', function(err) {
			notifier.notify({
				'title': 'What a fuck?',
				'message': err.message
			});
			console.log(err.message);
			return false;
		})
		.pipe(mincss({
			keepSpecialComments: 0,
			keepBreaks: false
		}))
		.pipe(rename('main.min.css'))
		.pipe(notify({
			'title': 'Less compilation',
			'message': 'Всё заебись!'
		}))
		.pipe(sourcemaps.write(''))
		.pipe(gulp.dest(path.build.css))
		.pipe(connect.reload());
});

gulp.task('minjs', function() {
	gulp.src(path.src.js)
		.pipe(rigger())
		// .pipe(debug({title: 'Js:'}))
		.pipe(minjs({comments: false}))
		.on('error', function(err) {
			notifier.notify({
				'title': 'What a fuck?',
				'message': err.message
			});
			console.log(err.message);
			return false;
		})
		.pipe(rename('app.min.js'))
		.pipe(notify({
			'title': 'JS compilation',
			'message': 'Всё заебись!'
		}))
		.pipe(gulp.dest(path.build.js))
		.pipe(connect.reload());
});

gulp.task('watch', function() {
	gulp.watch(path.watch.css, ['less']);
	gulp.watch(path.watch.twig, ['nunjucks']);
	gulp.watch(path.watch.js, ['minjs']);
	// gulp.watch(path.watch.html, ['html']);
});

gulp.task('default', ['connect', 'watch']);