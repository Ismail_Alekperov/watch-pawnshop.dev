$(document).ready(function () {
	'use strict';

	// WATCH-SLIDER
	(function() {
		var swiperWatch = new Swiper('.watch-slider', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			autoplay: 3500,
			effect: 'fade',
			fade: {
				crossFade: true
			}
		});
	})();

	// PRODUCT-LIST-SLIDER
	(function() {
		var sliders = [];
		$('.products').each(function(index, element){
			$(this).addClass('s' + index);
			var products = new Swiper('.products', {
				nextButton: '.swiper-button-next',
				prevButton: '.swiper-button-prev',
				// spaceBetween: 10,
				loop: true,
				slidesPerView: 'auto',
				roundLengths: true,
				observer: true,
				observeParents: true
			});

			if ($(window).width() <= 870) {
				$('.s' + index).swiper();
				// console.log($('.s' + index).swiper());
			}
		});
	})();

	// TOGGLE MENU
	(function() {
		var $menu = $('.menu'),
			$linkCategory = $('.js-category-show'),
			$linkServices = $('.js-services-show'),
			$subMenuCategory = $('#sub_menu_category'),
			$subMenuServices = $('#sub_menu_services'),
			$backCategory = $('.sub-menu__back_category'),
			$backServices = $('.sub-menu__back_services');

		$linkCategory.click(function() {
			$menu.fadeOut('fast');
			$subMenuCategory.fadeIn('fast');
			$back.addClass('sub-menu__back_in-category');
		});
		$linkServices.click(function() {
			$menu.fadeOut('fast');
			$subMenuServices.fadeIn('fast');
			$back.addClass('sub-menu__back_in-services');
		});
		$backCategory.click(function() {
			$menu.fadeIn('fast');
			$subMenuCategory.fadeOut('fast');
		});
		$backServices.click(function() {
			$menu.fadeIn('fast');
			$subMenuServices.fadeOut('fast');
		});
	})();

	// TOGGLE HEADER BODY
	(function() {
		var $headerBody = $('.header__body'),
			$toggleLink = $(".js-header-body");

		$toggleLink.click(function() {
			$headerBody.fadeIn('fast');
		});

		$headerBody.click(function() {
			event.stopPropagation();
		});

		$(document).click(function(e) {
			if (!$(e.target).hasClass("js-header-body")
				&& $(e.target).closest('.header__link-wrap').length === 0)
			{
				// console.log($(e.target));
				$headerBody.fadeOut('fast');
			}
		});
	})();

	// SHOW HIDE SEARCH IN FILTER
	(function() {
		var $searchTrigger = $('.filter__search-trigger'),
			$searchRow = $('.filter__search-row'),
			// $nav = $('.filter-nav'),
			// $sectionFilter = $('.section__filter'),
			$innerHead = $('.filter__head-inner-wrap'),
			$searchClear = $('.filter__search-clear');

			// console.log($sectionFilter);

		$searchTrigger.click(function(event) {
			var $t = $(this),
				searchRow = $t.closest('.filter').find('.filter__search-row'),
				// innerHead = $t.closest('.filter').find('.filter-nav'),
				inputSearch = $t.parent().find('input.js-search');

			// innerHead.css({'z-index': 'auto'});
			$innerHead.css({'z-index': 'auto'});
			$t.hide();
			searchRow.show();
			inputSearch.focus();

			event.stopPropagation();
		});
		$searchClear.click(function(event) {
			var $t = $(this),
			inputSearch = $t.parent().find('input.js-search');
			inputSearch.val('').focus();
		});

		$(document).click(function(e) {

			if ( e.target != $searchRow[0] && !$searchRow.has(e.target).length ) {
				$searchRow.hide();
				$searchTrigger.show();
				// $nav.css({'z-index': '3'});
				$innerHead.css({'z-index': '3'});
			}
		});
	})();

	// SHOW HIDE FILTER BODY
	(function() {
		var $filterShowLink = $('.filter__heading-list-link'),
			$filterHeadingItem = $('.filter__heading-list-item'),
			$filterBody = $('.filter__body'),
			$apply = $('#apply_filter');

		$filterHeadingItem.click(function(event) {
			var $t = $(this);

			$filterHeadingItem.removeClass('filter__heading-list-item_active');
			$t.addClass('filter__heading-list-item_active');
			$filterBody.slideDown('fast');
		});

		$apply.click(function(event) {
			$filterBody.slideUp('fast');
		});
	})();

	// ADVANTAGES SLIDER
	(function() {
		var $prevArrow = $('.advantages-slider__arrows_prev');
			$prevArrow.hide();

		var swiperAdvantages = new Swiper('.advantages-slider', {
			slidesPerView: 'auto',
			paginationClickable: true,
			spaceBetween: 20,
			nextButton: '.advantages-slider__arrows_next',
			prevButton: '.advantages-slider__arrows_prev',
			onSlideChangeEnd: function () {
				if ($(window).width() >= 870) {
					$prevArrow.show();
					if(swiperAdvantages.activeIndex==0){
						$prevArrow.hide();
					}
				}
			},
			breakpoints: {
				470: {
					spaceBetween: 10
				}
			}
		});
	})();

	// WIDE-SLIDER
	(function() {
		var swiperWide = new Swiper('.wide-slider', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			nextButton: '.wide-slider__arrows_next',
			prevButton: '.wide-slider__arrows_prev'
		});
	})();

	// SERVICES-HOVER
	(function() {
		var $clone,

		mouseenter = function() {
			var $t = $(this);

			$clone = $t.clone();
			$clone.addClass("services__item_hover").hide();
			$clone.fadeIn(300);

			$t.append($clone);
			return false;
		};

		$(document).one({
			mouseenter: mouseenter
		}, ".services__item");

		$(document).on({
			mouseleave: function() {
				var $t = $(this);

				$clone = null;
				$(".services__item_hover").remove();

				$(document).one({
					mouseenter: mouseenter
				}, ".services__item");

				return false;
			}
		}, ".services__item");

	})();

	// TABS
	(function() {
		$(".filter-nav__link")
			.click(function(event) {
				var $t = $(this),
					tab = $t.attr("href"),
					content = $t.closest('.filter').find('.filter__content');
				event.preventDefault();
				$t.parent().addClass("filter-nav__item_current");
				$t.parent().siblings().removeClass("filter-nav__item_current");
				content.not(tab).css({
					"display": "none"
				});
				$(tab).css({
					"display": "block"
				});

				if (tab == '#tab_2') {
					$('.js-focus').focus();
				}
			});
	})();


	// POPUPS
	(function() {
		var $popupLink = $('.js-popup-link');

		window.openPopup = function(href) {
			var $href;

			if (typeof href !== 'string') {
				console.log('Target does not defined');
				return false;
			}

			$href = $(href);
			if ($href.length < 1) {
				console.log('Target does not exist');
				return false;
			}

			$href.arcticmodal({
				overlay: {
					css: {
						backgroundColor: 'rgba(25, 51, 62, 0.2)',
						opacity: 1
					}
				},
				openEffect: {
					type: 'none',
					speed: 0
				},
				closeEffect: {
					type: 'none',
					speed: 0
				},
				afterOpen: function(asd) {
					(function() {
						//Main Swiper
						var swiperProduct = new Swiper('.product-gallery', {
							// loop: true,
							onSlideChangeStart: function(swiper){
								$('.product-thumbs__item').removeClass('product-thumbs__item_active');
								$('.product-thumbs__item').eq(swiper.realIndex).addClass('product-thumbs__item_active');
								// console.log(swiper.realIndex);
							}
						});

						// product-thumbs
						$('.product-thumbs__item').click(function(){
							swiperProduct.slideTo($(this).index());
							$('.product-thumbs__item').removeClass('product-thumbs__item_active');
							$(this).addClass('product-thumbs__item_active');
							console.log($(this).index());
						});
					})();
				},
				afterClose: function(data, el) {}
			});

			return false;
		};

		$popupLink.click(function() {
			var $t = $(this),
				href = $t.attr('data-href');

			$.arcticmodal('close');

			openPopup(href);
			return false;
		});
	})();

	// FILE INPUT
	(function() {
		$('input[type="file"]').change(function(){
			var $t = $(this),
				cover = $t.parent().find('.form__upload-cover');

			if (this.files && this.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					var selectedImage = e.target.result;
					cover
						.addClass('form__upload-cover_filled')
						.css({'background-image': 'url(' + selectedImage + ')'});
				};
				reader.readAsDataURL(this.files[0]);

				$t.closest('.form__field').next().show();

				// console.log();
			}
		});
	})();

	// TOGGLE FORM BUY
	(function() {
		var $buyBlock = $('.buy-block'),
			$priceBox = $('.buy-block__price'),
			$formBox = $('.buy-block__form'),
			$input = $('.form__input_app'),
			$link = $('.buy-block__link');

		$link.click(function() {
			var formBoxHeight = $formBox.innerHeight();
			$priceBox.fadeOut('fast');
			$formBox.fadeIn('fast');
			$buyBlock.css({'height': formBoxHeight});
			$input.focus();
		});

		$(document).click(function(e) {

			if ( e.target != $buyBlock[0] && !$buyBlock.has(e.target).length ) {
				$buyBlock.css({'height': 'auto'});
				$priceBox.fadeIn('fast');
				$formBox.fadeOut('fast');
			}
		});
	})();

	// PRODUCT-PAGE SLIDER
	(function() {
		//Main Swiper
		var productPageSlider = new Swiper('.product-page-slider', {
			loop: true,
			onSlideChangeStart: function(swiper){
				$('.product-page-thumbs__item').removeClass('product-page-thumbs__item_active');
				$('.product-page-thumbs__item').eq(swiper.realIndex).addClass('product-page-thumbs__item_active');
				// console.log(swiper.realIndex);
			}
		});

		// product-thumbs
		$('.product-page-thumbs__item').click(function(){
			productPageSlider.slideTo($(this).index() + 1);
			$('.product-page-thumbs__item').removeClass('product-page-thumbs__item_active');
			$(this).addClass('product-page-thumbs__item_active');
			// console.log($(this).index());
		});
	})();

	// PRICE FORM INPUT
	(function() {
		var $priceWrap = $('.price-form__price-wrap'),
			$clear = $('.price-form__clear');

		$clear.click(function(event) {
			var $t = $(this),
				input = $t.closest('.price-form__price-wrap').find('input');

			input.val('').focus();
		});
	})();

	(function() {
		lightGallery(document.getElementById('contcats_gallery'), {});
	})();
});